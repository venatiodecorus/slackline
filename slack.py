#! /usr/bin/python

# Calculates slackline values for slackliners
# Original equations by Bradley Duling
# Translated to Python by R. Anthony Lomartire
import cgi

form = cgi.FieldStorage()

length = int(form["length"].value)
sag = int(form["sag"].value)
weight = int(form["weight"].value)

sheaves = int(form["sheaves"].value)
pulleyeff = int(form["pulleyeff"].value) / 100.0
breakeff = int(form["breakeff"].value) / 100.0
stretch = int(form["stretch"].value) / 100.0

half_length = length / 2 # A24
btwofo = (((length/2)**2+sag**2)**0.5) # B24

# HTML info
print "Content-type: text/html"
print "\n\n"
print "<html><head><title>Output</title><head>"
print "<body>"
print "<link rel='stylesheet' type='text/css' href='style.css'>"

# Div to hold all our output
print "<div id='output'>"

# We're gonna use a table to format the output
print "<table>"

# Output values
tension =(weight*(length/2)*(((length/2)**2+sag**2)**0.5))/(sag*length)
print "<tr>"
print "<td class='name'>Line Tension:</td>" 
print "<td class='value' style='background-color:#ffff33'>" + "%0.2f" % tension + "</td>"
print "<td class='tip'><a>Static force with non-moving slacker in middle of line</a></td>"
print "</tr>"

line_stretch = (btwofo - half_length) / half_length
print "<tr>"
print "<td class='name'><a>Line Stretch with Slacker(%):</td>"
print "<td class='value'>" + "%0.3f" % (line_stretch * 100) + "%</td>" # Multiply for percentage
print "<td class='tip'><a>Stretch of slackline with slacker %</a></td>"
print "</tr>"

line_stretch_length = line_stretch * length
print "<tr>"
print "<td class='name'><a>Line Stretch with Slacker(length):</td>"
print "<td class='value'>" + "%0.2f" % line_stretch_length + "</td>"
print "<td class='tip'><a>Stretch of slackline with slacker length</a></td>"
print "</tr>"

mech_advantage = pulleyeff * breakeff
print "<tr>"
print "<td class='name'><a>Mechanical Advantage Efficiency:</td>"
# This is a little trick to get rounding to work I found on Stack Overflow
print "<td class='value'>" + "%i" % round((mech_advantage * 100)+.5) + "%</td>" # Percentage
print "<td class='tip'><a>Efficiency of pulley system with brake</a></td>"
print "</tr>"

theoretical_ma = sheaves + 1
print "<tr>"
print "<td class='name'><a>Theoretical MA:</td>"
print "<td class='value'>" + "%i" % theoretical_ma + "</td>"
print "<td class='tip'><a>Highest possible mechanical advantage with system</a></td>"
print "</tr>"

eff_loss_ma = theoretical_ma * mech_advantage
print "<tr>"
print "<td class='name'><a>Efficiency Loss MA:</td>"
print "<td class='value'>" + "%0.2f" % eff_loss_ma + "</td>"
print "<td class='tip'><a>Actual mechanical advantage with pulley system and break</a></td>"
print "</tr>"

unstretched_length = length - (length * stretch)
print "<tr>"
print "<td class='name'><a>Line Length(unstretched):</td>"
print "<td class='value'>" + "%i" % unstretched_length + "</td>"
print "<td class='tip'><a>Minimum length of slackline required for final (stretched) length</a></td>"
print "</tr>"

min_pulley_throw = length - unstretched_length
print "<tr>"
print "<td class='name'><a>Minimum Pulley Throw(length):</td>"
print "<td class='value'>" + "%i" % min_pulley_throw + "</td>"
print "<td class='tip'><a>Minimum amount of pulley length required to tension line to desired length</a></td>"
print "</tr>"

min_pulley_rope = min_pulley_throw * theoretical_ma
print "<tr>"
print "<td class='name'><a>Minimum Pulley Rope Length:</td>"
print "<td class='value'>" + "%i" % min_pulley_rope + "</td>"
print "<td class='tip'><a>Minimum length of static rope required for desired tension/length</a></td>"
print "</tr>"

pull_force_simple = tension / eff_loss_ma
print "<tr>"
print "<td class='name'><a>Pull Force Simple MA:</td>"
print "<td class='value'>" + "%i" % round(pull_force_simple+.5) + "</td>"
print "<td class='tip'><a>Force required to tension system (at brake)</a></td>"
print "</tr>"

pull_force_mult = pull_force_simple / ( 3 * ((pulleyeff + breakeff)/2) )
print "<tr>"
print "<td class='name'><a>Pull Force Multiplied MA:</td>"
print "<td class='value'>" + "%i" % pull_force_mult + "</td>"
print "<td class='tip'><a>Force required to tension system (multiplied through brake)</a></td>"
print "</tr>"

# End the table
print "</table>"

# End output div
print "</div>"

print "</body></html>"